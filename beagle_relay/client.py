import time
from relay import Relay


if __name__ == '__main__':
    # publish a single message
    relay = Relay('192.168.0.103')
    relay.open()
    time.sleep(2)
    relay.close()
