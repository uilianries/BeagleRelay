import paho.mqtt.client as mqtt
import syslog

def syslog_info(message):
    syslog.syslog(syslog.LOG_INFO, message)

def on_message(mqttc, userdata, msg):
    syslog_info("received - topic: %s - qos: %s - message: %s" % (msg.topic, str(msg.qos), str(msg.payload)))
    if str(msg.payload) == "open":
        with open('/sys/class/gpio/gpio60/value', 'w') as f:
            f.write('1')
    elif str(msg.payload) == "close":
        with open('/sys/class/gpio/gpio60/value', 'w') as f:
            f.write('0')

def on_publish(mqttc, userdata, msg):
    pass

if __name__ == "__main__":
    with open('/sys/class/gpio/gpio60/direction', 'w') as f:
        f.write('out')

    mqttc = mqtt.Client()
    mqttc.on_publish = on_publish
    mqttc.on_message = on_message
    mqttc.connect(host='localhost', port=1883, keepalive=60, bind_address="")
    mqttc.subscribe("device/relay", 0)
    mqttc.loop_forever()
